public class Main {
    public static void main(String[] args){
        int year = getFullYear();
        System.out.println(year + " " + getMonth() + LeapYear(year));
    }
    static String getRequestedLine(String requestMessage){
        Requester requester = new Requester();
        return requester.requestLine(requestMessage);
    }
    static boolean isNumeric(String string){
        return string.matches("-?\\d+(\\.\\d+)?");
    }

    static int getFullYear(){
        String year = getRequestedLine("Enter full year (example: 1993)").trim();
        return (isNumeric(year) && Integer.parseInt(year) > 0) ? Integer.parseInt(year) : getFullYear();
    }
    static String getMonth(){
        final String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        String month = getRequestedLine("Enter month number (between 1 and 12)").trim();
        return (isNumeric(month) && Integer.parseInt(month) >= 1 && Integer.parseInt(month) <= 12) ? months[Integer.parseInt(month) - 1] : getMonth();
    }

    static String LeapYear(int year){
        return (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) ? " - leap year" : " - not leap year";
    }

}
